import argparse, json, os
from flask import Flask, request, make_response
from flask_restful import reqparse, abort, Api, Resource
from pathlib import Path

STORAGE_FILE_PATH = os.getcwd()+'/tmp/storage.data'

def read_storage_file(file):
    data = json.loads('{}')
    try:
        with open(file, 'r', encoding = 'utf-8') as f:
            data = json.load(f)
    except IOError as e:
        print("Read storage file: I/O error(%s): %s" % (e.errno, e.strerror))
        exit(1)
    except FileNotFoundError:
        pass               
    finally:
        return data         



def save_storage_file(file,data):
    _tmp_dir = Path(os.getcwd()+'tmp')
    _tmp_dir.mkdir(parents=True, exist_ok=True)
    _file = Path(file)
    _file.touch(exist_ok=True)
    try:
        with open(_file, 'w+', encoding = 'utf-8') as f:
            json.dump(data, f)
    except IOError as e:
        print("Save storage file: I/O error(%s): %s %s" % (e.errno, e.strerror,STORAGE_FILE_PATH))
        exit(1)
    except:
        print("Save storage file: Unknow error")
        exit(1)


class Index(Resource):

    def get(self):
        html = """
        <!doctype html>

        <html lang="en">
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>AStorage Index</title>

        </head>

        <body>
        <h1>Key value storage</h1>
        <br>Получить все значения:
        <br>http://{HOSTNAME}/api/storage/json/all
        <br>
        <br>Получить данные по ключу:
        <br>http://{HOSTNAME}/api/storage/json/read?key=test
        <br>
        <br>Добавить значение:
        <br>curl -H 'Content-Type: application/json' -d "{\\"KEY1\\":\\"a777\\"}" -X POST http://{HOSTNAME}/api/storage/json/write
	
        </body>
        </html>
        """
        resp = make_response(html,200)
        resp.headers['Content-Type'] = 'text/html'
        return  resp

class Storage(Resource):
    def __init__(self, **kwargs):
        self.get_request_allowed = kwargs.get("get", False)
        self.post_request_allowed = kwargs.get("post", False)

    def get(self):
        if not self.get_request_allowed:
            return {'Error':'Bad request'}, 400
        
        _key = request.args.get("key")
  
        if _key:
            if _storage_data.get(_key):
                return {'result': ','.join(_storage_data.get(_key))}
            else:
                return {'Error':'Key `%s` not found' % _key}
        else:
            if _storage_data:
                return_all = []
                for key,values in _storage_data.items():
                    for v in values:
                        return_all.append({key:v})
                return return_all        
            else:
                return 'None'

    def post(self):
        if not self.post_request_allowed:
            return {'Error':'Bad request'}, 400
        ret = []
        if request.get_json():
            for k,v in request.get_json().items(): 
                
                if k in _storage_data:
                    _storage_data[k] += [v]
                else:
                    _storage_data[k] = [v]
  
            save_storage_file(STORAGE_FILE_PATH, _storage_data)
            return 'Success' , 201
        else:    
            return {'Error':'Bad parameters %s' %  request.get_json()} , 400





if __name__ == "__main__":
    _storage_data = read_storage_file(STORAGE_FILE_PATH)
    app = Flask(__name__)
    api = Api(app)
    api.add_resource(Storage, '/api/storage/json/all', endpoint = 'storage_all', resource_class_kwargs={"get": True})
    api.add_resource(Storage, '/api/storage/json/read', endpoint = 'storage_read',resource_class_kwargs={"get": True})
    api.add_resource(Storage, '/api/storage/json/write', endpoint = 'storage_write', resource_class_kwargs={"post": True})
    api.add_resource(Index, '/')
    app.run()


