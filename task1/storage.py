import argparse, json

STORAGE_FILE_PATH = './storage.data'


def read_storage_file(file):
    data = json.loads('{}')
    try:
        with open(file, 'r', encoding = 'utf-8') as f:
            data = json.load(f)
    except IOError as e:
        print("Read storage file: I/O error(%s): %s" % (e.errno, e.strerror))
        exit(1)
    except FileNotFoundError:
        pass    
    finally:
        return data         



def save_storage_file(file,data):
    try:
        with open(file, 'w', encoding = 'utf-8') as f:
            json.dump(data, f)
    except IOError as e:
        print("Save storage file: I/O error(%s): %s" % (e.errno, e.strerror))
        exit(1)
    except:
        print("Save storage file: Unknow error")
        exit(1)

  

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--key', type = str, required=True, help="Gey value by keys")
    parser.add_argument('--val', type = str, default=None,  help="Add value to key`")
    args = parser.parse_args()

    _key = args.key
    _value  = args.val

    _storage_data = read_storage_file(STORAGE_FILE_PATH)
    if _value:
        if _key in _storage_data:
            _storage_data[_key] += [_value]
        else:
            _storage_data[_key] = [_value]
        save_storage_file(STORAGE_FILE_PATH, _storage_data)
        print('Add new value for key: %s:%s' %(_key,_value))
    else:
        if _storage_data.get(_key): 
            print (','.join(_storage_data[_key]))
        else:    
            print('None')

